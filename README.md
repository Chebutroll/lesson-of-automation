Completely automated farming solution for "TribalWars" game (browser strategy).
Can be configured to run on different server-groups:

1. Language-specific data is controlled in 'bot.app.locale.py'.
2. Server-specific data (server speed, etc.) is controlled in 'settings.py'.

To automatically handle the CAPTCHA, bot uses Antigate service (antigate.com),
so valid Antigate API key should be set in 'settings.py'.

To run the bot:

1. Python3 is needed.
2. Install dependencies with 'pip install -r requirements.txt".
3. Adjust settings in 'settings.py' file.
4. Run 'python runner.py' command.

For now, bot was tested on .net "TribalWars" servers with Debian 7.3/Chromium,
but there are good chances ( :) ) that it will run smoothly on Debian OS family
(Ubuntu, etc.) and Windows (Vista, 7) with Chrome/Chromium browsers.
